//
//  AppController.h
//  JaFinance
//
//  Created by Jannick Hemelhof on 20/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Externs.h"

@class PreferenceController;
NSString *const ExpEmptyDocKey           = @"EmptyDocumentFlag";
NSString *const ExpOpenLastDocKey        = @"OpenLastDocFLag";
NSString *const ExpDefaultCategoriesKey  = @"DefaultCategoriesFlag";
NSString *const ExpDefaultCategoriesList = @"DefaultCategoriesList";
NSString *const ExpDefaultDateOffset     = @"DefaultDateOffset";

@interface AppController : NSObject {
    PreferenceController *preferenceController;
}

- (IBAction)showPreferencePanel:(id)sender;

@end
