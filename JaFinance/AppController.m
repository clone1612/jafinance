//
//  AppController.m
//  JaFinance
//
//  Created by Jannick Hemelhof on 20/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import "AppController.h"
#import "externs.h"
#import "PreferenceController.h"

@implementation   AppController
+ (void) initialize {
    // create a dictionary for the 'factory' defaults
    NSMutableDictionary    *defaultValues = [   NSMutableDictionary      dictionary  ];
    
    // First to create an array of default Categories (for now we will just make a few)
    NSMutableDictionary *catOne   = [[NSMutableDictionary alloc] initWithCapacity: 1];
    NSMutableDictionary *catTwo   = [[NSMutableDictionary alloc] initWithCapacity: 1];
    NSMutableDictionary *catThree = [[NSMutableDictionary alloc] initWithCapacity: 1];
    NSMutableDictionary *catFour  = [[NSMutableDictionary alloc] initWithCapacity: 1];
    NSMutableDictionary *catFive  = [[NSMutableDictionary alloc] initWithCapacity: 1];
    [catOne setValue: @"Housing" forKey: @"theString"];
    [catTwo setValue: @"Food" forKey: @"theString"];
    [catThree setValue: @"Entertainment" forKey: @"theString"];
    [catFour setValue: @"Misc" forKey: @"theString"];
    [catFive setValue: @"Transportation" forKey: @"theString"];
    
    NSArray *catArray = @[catOne, catTwo, catThree, catFour, catFive];
    
    // add defaults to dictionary
    defaultValues[ExpEmptyDocKey] = @YES;
    defaultValues[ExpDefaultCategoriesKey] = @YES;
    defaultValues[ExpOpenLastDocKey] = @NO;
    defaultValues[ExpDefaultDateOffset] = @06;
    defaultValues[ExpDefaultCategoriesList] = catArray;
    
    // register the defaults
    [[NSUserDefaults standardUserDefaults] registerDefaults: defaultValues];
}

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *) sender {
    return [[NSUserDefaults standardUserDefaults] boolForKey: ExpEmptyDocKey];
}

- (void)applicationDidFinishLaunching:(  NSNotification   *)aNotification {
    // see if we need to open the most recent document or not and do it if needed
    if ([[NSUserDefaults standardUserDefaults] boolForKey: ExpOpenLastDocKey]) {
        NSArray *anArray;
        NSDocumentController *docController = [NSDocumentController sharedDocumentController];
        anArray = [docController recentDocumentURLs];
        if (anArray != nil && [anArray count ] > 0)  {
            NSError *anError = nil;
            [docController openDocumentWithContentsOfURL:anArray[0] display: YES error:&anError];
        }
    }
}

- (IBAction) showPreferencePanel:(id)sender {
    if   (!preferenceController) {
        preferenceController= [[PreferenceController alloc] init];
    }
    [preferenceController showWindow: self];
}

@end
