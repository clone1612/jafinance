//
//  Document.h
//  JaFinance
//
//  Created by Jannick Hemelhof on 19/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class Expense;

@interface Document: NSPersistentDocument <NSToolbarDelegate> {
    IBOutlet NSTableView * expenseTable;
    IBOutlet NSTableView * categoryTable;
    IBOutlet NSTableView * expenseByCatTable;
    IBOutlet NSArrayController *categoryPopUpController;
    // Outlets for copy &amp; paste
    IBOutlet NSArrayController * expensesArrayController;
    IBOutlet NSSearchField *searchFieldOutlet;
    IBOutlet NSToolbar *toolbarOutlet;
}

- (IBAction) copy:(id) sender;
- (IBAction) paste:(id) sender;
- (NSArray *)rowArrayForItem:(Expense *)item;

@end

@interface Document(Toolbar)
- (void)setupToolbarForWindow:(NSWindow *)theWindow;

@end