//
//  Document.m
//  JaFinance
//
//  Created by Jannick Hemelhof on 19/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import "Document.h"
#import "externs.h"
#import "MSTablePrint.h"
#import "PrintTextView.h"

#import "Expense.h"
NSString *MSExpensesPBoardType = @"MSExpensesPBoardType" ;

@implementation Document

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
    }
    return self;
}

- (NSArray *)rowArrayForItem:(Expense *)item {
    // This method is only available in 10.6 and later for 10.5 follow the commented code below.
    NSString *dateString = [NSDateFormatter localizedStringFromDate:item.date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
    /*
     NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
     [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
     [dateFormatter setDateStyle:dateStyle];
     [dateFormatter setTimeStyle:timeStyle];
     NSString *dateString = [dateFormatter stringForObjectValue:item.date];
     */
    // This method is also only available in 10.6 and later, for 10.5 refer to the code above for the date formatter making substitutions as needed.
    NSString *amountString = [NSNumberFormatter localizedStringFromNumber:item.amount numberStyle:NSNumberFormatterCurrencyStyle];
    NSString *descriptionString = item.desc;
    NSString *categoryString = item.category.name;
    return @[dateString, amountString, descriptionString, categoryString];
    // The order here will match the order in the text table
}

- (BOOL) configurePersistentStoreCoordinatorForURL: (NSURL *)url ofType: (NSString *) fileType modelConfiguration: (NSString *) configuration storeOptions: (NSDictionary *) storeOptions error: (NSError **)error {
    NSMutableDictionary *options = nil;
    if (storeOptions != nil) {
        options = [storeOptions mutableCopy];
    }
    else {
        options = [[NSMutableDictionary alloc] init];
    }
    options[NSMigratePersistentStoresAutomaticallyOption] = @YES;
    // Add this line for simple migrations in Snow Leopard
    options[NSInferMappingModelAutomaticallyOption] = @YES;
    BOOL result = [super configurePersistentStoreCoordinatorForURL:url ofType:fileType modelConfiguration:configuration storeOptions:options error:error];
    options = nil;
    return result;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"Document";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)windowController {
    [super windowControllerDidLoadNib: windowController];
    // user interface preparation code
    // create two sort descriptors, one for date and one for name
    NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey: @"date" ascending: YES];
    NSSortDescriptor *nameSort = [[NSSortDescriptor alloc] initWithKey: @"name" ascending: YES];
    // Put the sort descriptors into arrays
    NSArray *dateDescriptors = @[dateSort];
    NSArray *nameDescriptors = @[nameSort];
    // Now set the corrent sort descriptors for each outlet
    // First set the tables that shows expenses to the date descriptor
    [expenseTable setSortDescriptors: dateDescriptors];
    [expenseByCatTable setSortDescriptors: dateDescriptors];
    // Now set the descriptors for the Category table
    [categoryTable setSortDescriptors: nameDescriptors];
    // For the Category popup button we have to sort the array controller not the button.
    [categoryPopUpController setSortDescriptors: nameDescriptors];
    [self setupToolbarForWindow:[windowController window]];
}

// For duplicating Expense entities
- (IBAction) copy:(id) sender {
    NSArray *selectedObjects = [expensesArrayController selectedObjects];
    NSUInteger count = [selectedObjects count];
    if (count == 0) {
        return ;
    }
    NSMutableArray *copyObjectsArray = [NSMutableArray arrayWithCapacity: count];
    NSMutableArray *copyStringsArray = [NSMutableArray arrayWithCapacity: count];
    
    for (Expense *expense in selectedObjects) {
        [copyObjectsArray addObject: [expense dictionaryRepresentation]];
        [copyStringsArray addObject: [expense stringDescription]];
    }
    
    NSPasteboard *generalPasteboard = [NSPasteboard generalPasteboard];
    [generalPasteboard declareTypes: @[MSExpensesPBoardType , NSStringPboardType] owner: self];
    NSData *copyData = [NSKeyedArchiver archivedDataWithRootObject: copyObjectsArray];
    [generalPasteboard setData: copyData forType: MSExpensesPBoardType];
    [generalPasteboard setString: [copyStringsArray componentsJoinedByString: @"\n"] forType: NSStringPboardType];
}

- (IBAction) paste:(id) sender {
    NSPasteboard *generalPasteboard = [NSPasteboard generalPasteboard];
    NSData *data = [generalPasteboard dataForType: MSExpensesPBoardType];
    if (data == nil) {
        return ;
    }
    NSArray *expensesArray = [NSKeyedUnarchiver unarchiveObjectWithData: data];
    NSManagedObjectContext *moc = [self managedObjectContext];
    NSArray *stringArray = [[generalPasteboard stringForType: NSStringPboardType] componentsSeparatedByString: @"\n"];
    NSEntityDescription *cats = [NSEntityDescription entityForName: @"Categoryjh" inManagedObjectContext: moc];
    NSString *predString = [NSString stringWithFormat: @"%@ LIKE %%@" , @"name"];
    int i = 0 ;
    for (NSDictionary *expenseDictionary in expensesArray) {
        //create a new Expense entity
        Expense *newExpense;
        newExpense = (Expense *)[NSEntityDescription insertNewObjectForEntityForName: @"Expense" inManagedObjectContext: moc];
        // Dump the values from the dictionary into the new entity
        [newExpense setValuesForKeysWithDictionary: expenseDictionary];
        // create a fetch request to get the category whose title matches the one in the array at the current index
        NSFetchRequest *req = [[NSFetchRequest alloc] init];
        // set the entity
        [req setEntity:cats];
        // create the predicate
        NSPredicate *predicate = [NSPredicate predicateWithFormat: predString, stringArray[i]];
        // set the predicate
        [req setPredicate:predicate];
        // just in case
        NSError *error = nil ;
        // execute the request
        NSArray *fetchResults = [moc executeFetchRequest: req error:&error];
        // acquire a pointer for the correct category
        Categoryjh *theCat = fetchResults[0];
        // get the expenses set from the category
        NSMutableSet *aSet = [theCat mutableSetValueForKey: @"expenses"];
        // now to add the new expense entity to the category
        [aSet addObject:newExpense];
        
        i++;
    } 
}

// Called only when document is first created
- (id) initWithType: (NSString *) typeName error: (NSError **) outError {
    // call the designated initalizer
    Document *document = [self init];
    
    // pass on the file type
    [self setFileType: typeName];
    
    // disable undo tracking
    NSManagedObjectContext *context = [self managedObjectContext];
    [[context undoManager] disableUndoRegistration];
    
    // check if the user want new documents to have basic entities added
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey: ExpDefaultCategoriesKey]) {
        // if the user wants the default items we hand them over
        // Add the default entities
        NSArray *categoryArray = [defaults objectForKey: ExpDefaultCategoriesList];
        NSDictionary *dictionary;
        for (dictionary in categoryArray) {
            Categoryjh *newCategory = [NSEntityDescription insertNewObjectForEntityForName: @"Categoryjh" inManagedObjectContext: [self managedObjectContext]];
            [newCategory setValue: dictionary[@"theString"] forKey: @"name"];
        }
    }
    
    // enable undo tracking
    [context processPendingChanges];
    [[context undoManager] enableUndoRegistration];
    
    return document;
}

- (void)add:(id)sender {
    [expensesArrayController add:sender];
}

- (void)delete:(id)sender {
    [expensesArrayController remove:sender];
}

#pragma mark Printing
- (NSPrintOperation *)printOperationWithSettings:(NSDictionary *)printSettings error:(NSError **)outError {
    NSPrintInfo *pInfo = [self printInfo];
    [pInfo setHorizontalPagination:NSFitPagination]; // This will keep the text table from spaning more than one page across.
    [pInfo setVerticallyCentered:NO]; // Keep the table from ending up in the middle of the page. Not that big of a deal for one page, but the last page looks odd without this.
    [[pInfo dictionary] setValue:@YES forKey:NSPrintHeaderAndFooter]; // Add header and footer to all pages
    [[pInfo dictionary] addEntriesFromDictionary:printSettings]; // Add any settings that were passed in
	PrintTextView *printView = [[PrintTextView alloc] initWithFrame:[pInfo imageablePageBounds]]; // Create a text view with one page as its size
    printView.printJobTitle = @"Expense Report"; // The name used as default for save as PDF and for the default header
    MSTablePrint *tPrint = [[MSTablePrint alloc] init];
	// Set up the fetch request to retrive all Expense entities
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Expense" inManagedObjectContext:[self managedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *anError = nil;
    NSArray *items = [[self managedObjectContext] executeFetchRequest:request error:&anError];
	
    NSArray *headerArray = @[@"Date", @"Amount", @"Description", @"Category"]; // create the first row of the text table with headers
    NSMutableArray *itemArray = [[NSMutableArray alloc] init]; // create the array we will be passing to MSTablePrint
    [itemArray addObject:headerArray]; // whatever is at index 0 of this array will become the first row so we add the headers here
    for (Expense *z in items) { // Step through each Expense entity in the array and create an array to represent the entity.
        [itemArray addObject:[self rowArrayForItem:z]];
    }
    NSAttributedString *atString = [tPrint attributedStringFromItems:itemArray]; //create the text table
    [[printView textStorage] setAttributedString:atString]; // set the text table as the only text in the text view
    NSPrintOperation *printOp = [NSPrintOperation printOperationWithView:printView printInfo:pInfo]; // Setup the actual print operation
    return printOp;
}


+ (BOOL)autosavesInPlace
{
    return YES;
}

@end
