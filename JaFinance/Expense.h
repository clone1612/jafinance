//
//  Expense.h
//  JaFinance
//
//  Created by Jannick Hemelhof on 19/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Categoryjh.h"

@interface Expense : NSManagedObject {
    
}

@property (nonatomic, strong) NSNumber * amount;
@property (nonatomic, strong) NSDate * date;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) Categoryjh *category;

+ (NSArray *) keysToBeCopied;
- (NSDictionary *) dictionaryRepresentation;
- (NSString *) stringDescription;

@end
