//
//  Expense.m
//  JaFinance
//
//  Created by Jannick Hemelhof on 19/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Expense.h"


@implementation Expense

@dynamic amount;
@dynamic date;
@dynamic desc;
@dynamic category;

- (void) awakeFromInsert {
    NSNumber *days = [[NSUserDefaults standardUserDefaults] objectForKey: @"DefaultDateOffset"];
    int dayInt = [days intValue];
    if (dayInt ==0) {
        NSDate *now = [NSDate date];
        self.date = now;
    }
    else {
        int seconds = dayInt * 86400;   // number of days times number of seconds in one day.
        NSDate *theDate = [[NSDate alloc] initWithTimeIntervalSinceNow: seconds];
        self.date = theDate;
        // we called alloc therefore we call release, even though it will be released when this method ends anyway.
        //[theDate release];
    }
}

// Copy/Paste methods
+ (NSArray *) keysToBeCopied {
    static NSArray *keysToBeCopied = nil ;
    if (keysToBeCopied == nil) {
        // This will determine which attributes get copied. Must NOT copy relationships or it will copy the actual entity
        // Date has been left out so that the date will default to the current date.
        keysToBeCopied = @[@"desc" , @"amount"];
    }
    return keysToBeCopied;
}

- (NSDictionary *) dictionaryRepresentation {
    return [self dictionaryWithValuesForKeys: [[self class] keysToBeCopied]];
}

- (NSString *) stringDescription {
    // This will return the title of the category as a string
    NSString *stringDescription = nil ;
    Categoryjh *category = self.category;
    if (category != nil) {
        stringDescription = category.name ;
    }
    return stringDescription;
}

@end
