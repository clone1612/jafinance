//
//  PreferenceController.m
//  JaFinance
//
//  Created by Jannick Hemelhof on 20/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import "PreferenceController.h"

@interface PreferenceController ()

@end

@implementation PreferenceController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (id)init {
    if (![ super initWithWindowNibName: @"Preferences"]) {
        return nil;
    }
    return self;
}

@end
