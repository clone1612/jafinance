/*
 externs.h
 This is just a collection of all externs for the project.
 */

extern NSString *const ExpEmptyDocKey;           // To determine if we should oopen a blank document at launch
extern NSString *const ExpOpenLastDocKey;        // To determine if we should open the last opened document at launch
extern NSString *const ExpDefaultCategoriesKey;  // To determine if we should insert the default categories to new docs
extern NSString *const ExpDefaultCategoriesList; // The list of what categories should get added if above is YES
extern NSString *const ExpDefaultDateOffset;     // How far before the current date should expenses start at.