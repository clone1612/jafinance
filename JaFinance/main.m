//
//  main.m
//  JaFinance
//
//  Created by Jannick Hemelhof on 19/05/14.
//  Copyright (c) 2014 JaSoftware. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
