//
//  PrintTextView.h
//  Expenses
//
//  Created by Michael Swan on 8/2/10.
//  Copyright (c) 2010 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface PrintTextView : NSTextView {

    NSString *printJobTitle;
}
@property (copy, readwrite) NSString *printJobTitle;

@end
